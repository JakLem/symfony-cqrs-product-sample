<?php


namespace App\Shared\Application\Exception;


use JetBrains\PhpStorm\Pure;
use Throwable;

class InvalidBodyRequestPropertyException extends \Exception
{
    #[Pure] public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString(): string
    {
        return $this->message;
    }
}