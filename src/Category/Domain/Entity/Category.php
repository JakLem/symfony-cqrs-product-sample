<?php


namespace App\Category\Domain\Entity;


use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(name="product")
 */
class Category
{
    private UuidInterface $id;

    /**
     * @var string
     */
    private string $name;

    public function __construct()
    {
    }
}