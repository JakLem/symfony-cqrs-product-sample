<?php


namespace App\Product\Infrastructure\Saver;

use App\Product\Domain\Entity\Product;
use App\Product\Domain\Port\ProductSaver;
use Doctrine\ORM\EntityManagerInterface;

class ProductDatabaseSaver implements ProductSaver
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Product $product)
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }
}