<?php


namespace App\Product\Infrastructure\Controller;


use App\Product\Application\Message\ShowProductMessage;
use App\Product\Domain\Dto\View\ProductView;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

class ShowProductController
{
    #[Route("/product/{uuid}", name: "api_product_show", methods: ["GET"])]
    public function show(
        MessageBusInterface $messageBus,
        Request $request
    ) : JsonResponse
    {
        $uuid = $request->get('uuid');

        $envelope = $messageBus->dispatch(
            new ShowProductMessage($uuid)
        );

        $handledStamp = $envelope->last(HandledStamp::class);
        /** @var ProductView $productView */
        $productView = $handledStamp->getResult();
        return new JsonResponse($productView, 200);
    }
}
