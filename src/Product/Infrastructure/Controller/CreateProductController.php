<?php


namespace App\Product\Infrastructure\Controller;


use App\Product\Application\Form\ProductCreateType;
use App\Product\Application\Message\CreateProductMessage;
use App\Shared\Application\RequestBodyParser;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class CreateProductController extends AbstractController
{
    #[Route('/product', name: 'api_product_create', methods: ['POST'])]
    public function create(
        MessageBusInterface $messageBus,
        Request $request,
        RequestBodyParser $bodyParser
    ) : JsonResponse
    {
        $bodyParser->parse($request);
        $productUuid = Uuid::uuid4();

        $messageBus->dispatch(
          new CreateProductMessage(
              $productUuid,
              $bodyParser->name
          )
        );

        return new JsonResponse([
            'product' => $productUuid, 200
        ]);
    }
}