<?php


namespace App\Product\Infrastructure\Repository;

use App\Product\Domain\Dto\View\ProductView;
use App\Product\Domain\Entity\Product;
use App\Product\Domain\Port\ProductProvider;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class ProductDatabaseRepository  implements ProductProvider
{
    public function __construct(
        private EntityManagerInterface $entityManager
    )
    {}

    public function findByUuid(string $uuid) : ProductView
    {
        $qb = $this->entityManager->createQueryBuilder();
        $r = $qb->select('p.name')
            ->addSelect('p.id')
            ->andWhere('p.id = :uuid')
            ->setParameter("uuid", $uuid)
            ->from("App\Product\Domain\Entity\Product", "p")
            ->getQuery()
            ->getOneOrNullResult();


        return new ProductView($r["name"]);
    }
}