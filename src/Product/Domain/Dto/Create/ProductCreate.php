<?php


namespace App\Product\Domain\Dto\Create;


class ProductCreate
{
    public function __construct(
        public string $name
    )
    {
    }
}