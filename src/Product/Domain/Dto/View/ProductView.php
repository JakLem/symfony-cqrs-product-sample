<?php


namespace App\Product\Domain\Dto\View;


class ProductView
{
    public function __construct(
        public string $name
    ){}
}