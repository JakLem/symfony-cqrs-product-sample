<?php


namespace App\Product\Domain\Entity;

use App\Product\Infrastructure\Repository\ProductDatabaseRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=ProductDatabaseRepository::class)
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string", nullable=false)
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    public function __construct(
        UuidInterface $id,
        string $name
    )
    {
        $this->id = $id;
        $this->setName($name);
    }

    public function getId() : UuidInterface
    {
        return $this->id;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    public function getName() : string
    {
        return $this->name;
    }
}