<?php


namespace App\Product\Domain\Port;


interface ProductProvider
{
    public function findByUuid(string $uuid);
}