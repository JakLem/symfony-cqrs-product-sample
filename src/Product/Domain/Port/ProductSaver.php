<?php


namespace App\Product\Domain\Port;


use App\Product\Domain\Entity\Product;

interface ProductSaver
{
    public function save(Product $product);
}