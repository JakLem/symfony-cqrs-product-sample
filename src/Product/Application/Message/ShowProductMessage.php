<?php


namespace App\Product\Application\Message;

class ShowProductMessage
{
    public function __construct(
        public string $id
    ){}

    public function getId() : string
    {
        return $this->id;
    }
}