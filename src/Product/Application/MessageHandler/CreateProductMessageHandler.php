<?php


namespace App\Product\Application\MessageHandler;


use App\Product\Application\Message\CreateProductMessage;
use App\Product\Domain\Entity\Product;
use App\Product\Domain\Port\ProductSaver;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateProductMessageHandler implements MessageHandlerInterface
{
    /**
     * @var ProductSaver
     */
    private ProductSaver $productSaverDatabaseRepository;

    public function __construct(ProductSaver $productSaverDatabaseRepository)
    {
        $this->productSaverDatabaseRepository = $productSaverDatabaseRepository;
    }

    public function __invoke(CreateProductMessage $createProductMessage)
    {
        $product = new Product(
            $createProductMessage->getId(),
            $createProductMessage->getName()
        );

        $this->productSaverDatabaseRepository->save($product);
    }
}