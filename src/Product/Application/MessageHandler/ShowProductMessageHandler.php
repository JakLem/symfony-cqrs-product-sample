<?php


namespace App\Product\Application\MessageHandler;

use App\Product\Application\Message\ShowProductMessage;
use App\Product\Domain\Port\ProductProvider;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ShowProductMessageHandler implements MessageHandlerInterface
{
    /**
     * @var ProductProvider
     */
    private ProductProvider $productRepository;

    public function __construct(
        ProductProvider $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(ShowProductMessage $showProductMessage)
    {

        $product = $this->productRepository->findByUuid($showProductMessage->getId());

        return $product;
    }
}